module gitlab.com/luke.kennedy/space-engineers-base-placer

go 1.18

require (
	github.com/go-gl/mathgl v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/cobra v1.5.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
)
