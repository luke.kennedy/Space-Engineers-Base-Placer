package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"

	"github.com/go-gl/mathgl/mgl64"
	"github.com/spf13/cobra"
)

func main() {
	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			fileName, err := cmd.Flags().GetString("in")
			if err != nil {
				return err
			}
			data, err := ioutil.ReadFile(fileName)
			if err != nil {
				return err
			}

			r := csv.NewReader(bytes.NewReader(data))
			r.Comma = ':'
			r.FieldsPerRecord = 7
			records, err := r.ReadAll()
			if err != nil {
				return err
			}
			parsed, err := csvToRecords(records)
			if err != nil {
				return err
			}
			distances := buildDistances(parsed)

			closestResources, totals := findClosestResource(parsed, distances)

			sort.Slice(parsed, func(i, j int) bool {
				return totals[i] < totals[j]
			})

			for i, record := range parsed {
				log.Printf("Name: %s, Total Distance: %f, Coords: %v\nDistances: %v\n", record.RawName, totals[i], record.Location, closestResources[i])
			}

			return nil
		},
	}
	cmd.Flags().StringP("in", "i", "sample.csv", "-i ./path-to-file.csv")
	if err := cmd.Execute(); err != nil {
		log.Fatalf("%v", err)
	}
}

type gpsRecord struct {
	RawName  string
	Resource string
	Location mgl64.Vec3
}

func csvToRecords(in [][]string) ([]gpsRecord, error) {
	out := []gpsRecord{}
	for _, next := range in {
		xRaw := next[2]
		yRaw := next[3]
		zRaw := next[4]
		x, err := strconv.ParseFloat(xRaw, 64)
		if err != nil {
			return nil, fmt.Errorf("could not parse coordinate %s in record %v", xRaw, next)
		}
		y, err := strconv.ParseFloat(yRaw, 64)
		if err != nil {
			return nil, fmt.Errorf("could not parse coordinate %s in record %v", yRaw, next)
		}
		z, err := strconv.ParseFloat(zRaw, 64)
		if err != nil {
			return nil, fmt.Errorf("could not parse coordinate %s in record %v", zRaw, next)
		}
		loc := mgl64.Vec3{x, y, z}
		resources := strings.Split(next[1], " & ")
		for _, resource := range resources {
			record := gpsRecord{
				RawName:  next[1],
				Resource: resource,
				Location: loc,
			}
			out = append(out, record)
		}
	}
	return out, nil
}

func buildDistances(records []gpsRecord) (out map[int]map[int]float64) {
	out = map[int]map[int]float64{}
	for i := range records {
		for j := i + 1; j < len(records); j++ {
			a := records[i]
			b := records[j]
			distance := b.Location.Sub(a.Location).Len()
			if out[i] == nil {
				out[i] = map[int]float64{}
			}
			if out[j] == nil {
				out[j] = map[int]float64{}
			}
			out[i][j] = distance
			out[j][i] = distance
		}
	}
	return out
}

func findClosestResource(records []gpsRecord, distances map[int]map[int]float64) (out map[int]map[string]float64, totals map[int]float64) {
	out = map[int]map[string]float64{}
	totals = map[int]float64{}
	for i, record := range records {
		out[i] = map[string]float64{}
		for j, other := range records {
			if i == j || record.Resource == other.Resource {
				continue
			}
			newDistance := distances[i][j]
			currentDistance := out[i][other.Resource]
			if currentDistance == 0 || newDistance < currentDistance {
				out[i][other.Resource] = newDistance
			}
		}
		total := float64(0)
		for _, dist := range out[i] {
			total += dist
		}
		totals[i] = total
	}
	return
}
